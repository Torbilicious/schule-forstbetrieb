package gui;

import javafx.animation.AnimationTimer;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.Baum;
import model.Feld;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Torbe on 04.12.2015.
 */
public class View extends AnimationTimer {
    private final GraphicsContext gc;
    private final Feld feld;

    private ArrayList<Color> colors = new ArrayList<>();

    private Integer framesDrawn = 0;
    private Timer fpsTimer;

    private Double D = 0D;
    private Double B = 0D;
    private Integer fps = 0;

    private Boolean drawNumbers = true;

    public View(GraphicsContext gc, Feld feld) {
        this.gc = gc;
        this.feld = feld;

        colors.add(Color.ORANGE);
        colors.add(Color.RED);
        colors.add(Color.BLUE);
        colors.add(Color.YELLOW);
        colors.add(Color.DARKBLUE);

        fpsTimer = new Timer();
        fpsTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                fps = framesDrawn;
                D = feld.getD();
                B = feld.getB();

                framesDrawn = 0;
            }
        }, 0, 1000);
    }

    @Override
    public void handle(long now) {
        gc.clearRect(0, 0, feld.getDimension().getHeight(), feld.getDimension().getWidth());

        for (Baum baum : feld.getBäume()) {
            Point2D position = baum.getPosition();
            Double radius = baum.getRadius();

            if (!(baum.getArt().getId() > colors.size()))
                gc.setFill(colors.get(baum.getArt().getId())); //select color for tree species
            gc.fillOval(position.getX() - radius, position.getY() - radius, radius * 2, radius * 2); //draw tree

            gc.setStroke(Color.BLACK);
            gc.strokeOval(position.getX() - radius, position.getY() - radius, radius * 2, radius * 2); //draw border for tree

            gc.strokeText(Integer.toString(baum.getIndex()), position.getX(), position.getY());
        }

        gc.strokeText(getInfoText(), 5, 20);

        framesDrawn++;
    }

    private String getInfoText() {
        return String.format("Fps: %s\nD: %s\nB: %s", fps, D, B);
    }

//    private Double calculateFps(Long last, Long now)
//    {
//        return 1000D / (now - last);
//    }

    public void stopTimer() {
        fpsTimer.cancel();
    }
}
