package gui;

import algorithm.Algorithm;
import input.InputManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Dimension2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import model.Feld;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    TextArea txtArea;

    @FXML
    Label lblLegend;

    @FXML
    Button btnApply;

    @FXML
    CheckBox startTreeView;

    @FXML
    CheckBox generateGnuplotFile;
    @FXML
    StackPane generateGnuplotFilePane;

    @FXML
    CheckBox startGnuplot;
    @FXML
    StackPane startGnuplotPane;

    private Stage stage;

    public void applyPressed() {
        Feld feld = InputManager.setupForestForInput(txtArea.getText());
        stage.setOnHiding(event -> setupForestView(feld));
        stage.close();
    }

    private void setupForestView(Feld feld) {
        if (!startTreeView.isSelected()) {
            Platform.exit();
            return;
        }

        Thread run = new Algorithm(feld);

        run.start();

        Dimension2D dimension = feld.getDimension();

        Group group = new Group();
        Canvas canvas = new Canvas(dimension.getWidth(), dimension.getHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();
        group.getChildren().add(canvas);

        Stage stage = new Stage();
        stage.setTitle(feld.getWaldname());
        stage.setScene(new Scene(group, dimension.getWidth(), dimension.getHeight() + feld.windowToppingMargin));
        stage.setResizable(false);
        stage.setMinWidth(dimension.getWidth());
        stage.setMinHeight(dimension.getHeight() + feld.windowToppingMargin);
        stage.show();

        View treeView = new View(gc, feld);
        treeView.start();

        stage.setOnHiding(event -> {
            treeView.stopTimer();
            Algorithm.disable();
            Platform.exit();
        });
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lblLegend.setText("Name\n" +
                "Höhe Breite\n");

        String text = "";
        List<String> arguments = Main.parameters;

        for (int i = 0; i < arguments.size(); i++) {
            if (i != 0)
                text += '\n';

            text += arguments.get(i);
        }

        txtArea.setText(text);

        Tooltip tooltip = new Tooltip("Not yet implemented.");
        Tooltip.install(generateGnuplotFilePane, tooltip);
        Tooltip.install(startGnuplotPane, tooltip);
    }
}
