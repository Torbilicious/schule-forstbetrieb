package gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.List;

public class Main extends Application {
    public static List<String> parameters;

    @Override
    public void start(Stage primaryStage) throws Exception {
        parameters = getParameters().getUnnamed();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("addForrestScreen.fxml"));
        Parent group = loader.load();

        Controller controller = loader.getController();
        controller.setStage(primaryStage);

        primaryStage.setTitle("\uD83C\uDF32 \uD83C\uDF32 MATSE Grün AG \uD83C\uDF32 \uD83C\uDF32");
        primaryStage.setScene(new Scene(group));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
