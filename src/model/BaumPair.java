package model;

/**
 * Created by user on 07.03.2016.
 */
public class BaumPair
{
    Baum treeOne;
    Baum treeTwo;

    public BaumPair(Baum one, Baum two)
    {
        this.treeOne = one;
        this.treeTwo = two;
    }

    public Baum getTreeOne() {
        return treeOne;
    }

    public void setTreeOne(Baum treeOne) {
        this.treeOne = treeOne;
    }

    public Baum getTreeTwo() {
        return treeTwo;
    }

    public void setTreeTwo(Baum treeTwo) {
        this.treeTwo = treeTwo;
    }
}
