package model;

/**
 * Created by Torbe on 01.12.2015.
 */
public class Cluster {
    private final Double x;
    private final Double y;

    private Boolean besetzt = false;

    public Cluster(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Boolean isBesetzt() {
        return besetzt;
    }

    public void besetzt() {
        besetzt = true;
    }
}
