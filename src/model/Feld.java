package model;

import javafx.geometry.Dimension2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Torbe on 01.12.2015.
 */
public class Feld {
    private final String waldname;
    private final Dimension2D dimension;

    private Double D;
    private Double B;

    private ArrayList<Baum> bäume;
    private ArrayList<Cluster> cluster;
    private ArrayList<Baumart> baumarten;

    public final Integer windowToppingMargin = 45;

    public Feld(String waldname, Dimension2D dimension) {
        this.waldname = waldname;
        this.dimension = new Dimension2D(dimension.getWidth(), dimension.getHeight() - windowToppingMargin);

        bäume = new ArrayList<>();
        cluster = new ArrayList<>();
        baumarten = new ArrayList<>();
    }

    public Dimension2D getDimension() {
        return dimension;
    }

    public String getWaldname() {
        return waldname;
    }

    public ArrayList<Baum> getBäume() {
        return bäume;
    }

    public List<Baum> getLastTwoTrees() {
        List<Baum> lastTwoTrees = new ArrayList<>();

        if (bäume.size() > 1) {
            lastTwoTrees.add(bäume.get(bäume.size() - 2));
            lastTwoTrees.add(bäume.get(bäume.size() - 1));
        }

        return lastTwoTrees;
    }

//    /**
//     * Creates a new Tree and then appends it locally.
//     *
//     * @param x horizontal position of the new tree
//     * @param y vertical position of the new tree
//     * @param art species for the new tree
//     */
//    public void addBaum(Double x, Double y, Baumart art)
//    {
//        addBaum(new Baum(x, y, art));
//    }

    public void addBaum(Baum baum) {
        bäume.add(baum);
        baum.getArt().erhöheGesetzteBäume();
    }

    public void addBaumart(Baumart art) {
        baumarten.add(art);
    }

    public Double getD() {
        if (bäume.size() == 0)
            return 0D;

        Double tmp = 0D;

        for (Baumart aBaumarten : baumarten) {
            tmp += Math.pow(aBaumarten.getAnzahlBaeume() / bäume.size(), 2);
        }

        D = 1 - tmp;

        return D;
    }

    public Double getB() {
        if (bäume.size() == 0)
            return 0D;

        Double tmp = 0D;

        for (Baumart aBaumarten : baumarten) {
            tmp += aBaumarten.getAnzahlBaeume() * Math.PI * Math.pow(aBaumarten.getRadius(), 2);
        }

        B = D * tmp / (dimension.getHeight() * dimension.getWidth());

        return B;
    }

    public ArrayList<Baumart> getBaumarten() {
        return baumarten;
    }
}
