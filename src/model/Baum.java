package model;

import javafx.geometry.Point2D;
import javafx.scene.shape.Circle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Torbe on 01.12.2015.
 */
public class Baum {
    private int index;
    private final Circle circle;
    private final Baumart art;
    private List<Baum> neighbour = new ArrayList<>();

    public Baum(Double x, Double y, Baumart art, int index) {
        this.art = art;
        this.circle = new Circle(x, y, art.getRadius());
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public Baumart getArt() {
        return art;
    }

    public Point2D getPosition() {
        return new Point2D(circle.getCenterX(), circle.getCenterY());
    }

    public Double getRadius() {
        return art.getRadius();
    }

    public Circle getCircle() {
        return circle;
    }

    public Boolean checkCollision(Baum baum) {
//        Shape intersect = Shape.intersect(circle, baum.getCircle());
//        return intersect.getBoundsInLocal().getWidth() != -1;

//        return this.circle.getBoundsInParent().intersects(baum.getCircle().getBoundsInParent());

        Double dx = circle.getCenterX() - baum.getCircle().getCenterX();
        Double dy = circle.getCenterY() - baum.getCircle().getCenterY();

        Double radiusSum = getRadius() + baum.getRadius();

        return dx * dx + dy * dy <= radiusSum * radiusSum;
    }

    public void removeNachbar(Baum nachbar) {
        neighbour.remove(nachbar);
    }

    public List<Baum> getNeighbours() {
        return neighbour;
    }

    @Override
    public String toString() {
        return String.format("Baum%s: X: %s Y: %s", index, circle.getCenterX(), circle.getCenterY() );
    }
}
