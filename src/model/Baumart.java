package model;

/**
 * Created by Torbe on 01.12.2015.
 */
public class Baumart {
    private final Double radius;
    private final Integer id;
    private Integer gesetzteBäume = 0;

    public Baumart(Double radius, Integer id) {
        this.radius = radius;
        this.id = id;
    }

    public void erhöheGesetzteBäume() {
        gesetzteBäume++;
    }

    public Integer getId() {
        return id;
    }

    public Double getRadius() {
        return radius;
    }

    public Integer getAnzahlBaeume() {
        return gesetzteBäume;
    }
}
