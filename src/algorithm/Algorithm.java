package algorithm;

import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import model.Baum;
import model.BaumPair;
import model.Baumart;
import model.Feld;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by TBinder on 25/01/16.
 */

public class Algorithm extends Thread {
    private Feld feld;
    private ArrayList<Baum> allTrees = new ArrayList<>();
    private Queue<BaumPair> baumPairList = new LinkedList<>();

    private Double width;
    private Double height;

    private static Boolean running = true;

    private final NewTreeSpeciesGenerator speciesGenerator;
    private Integer currentTree = 1;

    public Algorithm(Feld feld) {
        this.feld = feld;
        this.speciesGenerator = new NewTreeSpeciesGenerator(feld);
        this.width = feld.getDimension().getWidth();
        this.height = feld.getDimension().getHeight();
    }

    public Baum setNextTree(Baum baumOne, Baum baumTwo, Baumart nextTree, int side) {

        double radiusBaumOne = baumOne.getRadius();
        double radisuBaumTwo = baumTwo.getRadius();

        double l1 = radisuBaumTwo + nextTree.getRadius();
        double l2 = radiusBaumOne + nextTree.getRadius();
        double l3 = radisuBaumTwo + radiusBaumOne;

        double angle = calculateAngle(l1, l2, l3);

        double sm3 = Math.sin(angle) * l2;
        double sm1 = Math.cos(angle) * l2;

        Point2D vektorMittelpunkt = normiereVektor(baumOne, baumTwo);
        double laenge = Math.sqrt(Math.pow(vektorMittelpunkt.getX(), 2) + Math.pow(vektorMittelpunkt.getY(), 2));
        Point2D s = getS(vektorMittelpunkt, sm1, baumOne, laenge);

        Point2D newPoint = getMittelpunktForNewTree(vektorMittelpunkt, sm3, s, laenge, side == 1);
        Baum nextBaum = new Baum(newPoint.getX(), newPoint.getY(), nextTree, currentTree);

        if (!isInBounds(nextBaum) || checkCollision(nextBaum)) {
            return null;
        }

        return nextBaum;
    }

    public Point2D normiereVektor(Baum baumOne, Baum baumTwo) {
        Point2D mittelpunktOne = baumOne.getPosition();
        Point2D mittelpunktTwo = baumTwo.getPosition();

        Point2D vektorMittelpunkt = new Point2D(mittelpunktTwo.getX() - mittelpunktOne.getX(), mittelpunktTwo.getY() - mittelpunktOne.getY());

        return vektorMittelpunkt;
    }

    public Point2D getS(Point2D vektorMittelpunkt, double sm1, Baum baumOne, double laenge) {
        // double laenge = Math.sqrt(Math.pow(vektorMittelpunkt.getX(),2) + Math.pow(vektorMittelpunkt.getY(),2));

        double normiert = sm1 / laenge;
        Double baumX = baumOne.getPosition().getX();
        Double baumY = baumOne.getPosition().getY();

        Point2D punktS = new Point2D(vektorMittelpunkt.getX() * normiert + baumX, vektorMittelpunkt.getY() * normiert + baumY);

        return punktS;
    }

    public Point2D getMittelpunktForNewTree(Point2D vektor, double sm3, Point2D s, double laenge, Boolean seite) {
        Point2D newVektor = new Point2D(((vektor.getY() * -1.0) / laenge) * sm3, vektor.getX() / laenge * sm3);
        // double laenge2 = Math.sqrt(Math.pow(vektor.getX(),2) + Math.pow(vektor.getY(),2));
        // double newLaenge = sm3/laenge2;

        Point2D newMittelpunkt;

        if (!seite) {
            newMittelpunkt = new Point2D(s.getX() - newVektor.getX(), s.getY() - newVektor.getY());
        } else {
            //newMittelpunkt = new Point2D(s.getX() + (newVektor.getX() * sm3), s.getY() + (newVektor.getY() * sm3));
            newMittelpunkt = new Point2D(newVektor.getX() + s.getX(), newVektor.getY() + s.getY());
        }

        return newMittelpunkt;
    }

    public double calculateAngle(double l1, double l2, double l3) {
        double zaehler = Math.pow(l2, 2) + Math.pow(l3, 2) - Math.pow(l1, 2);
        double nenner = 2 * l2 * l3;
        double angle = Math.acos(zaehler / nenner);

        return angle;
    }

    @Override
    public void run() {
        addTrees();
    }

    public Baum setFirstTree(Random r) {
        Baumart baumart = speciesGenerator.getNextSpecies();
        Double min = baumart.getRadius();

//        Double x = min + ((width - baumart.getRadius()) - min) * r.nextDouble();
//        Double y = min + ((height - baumart.getRadius()) - min) * r.nextDouble();
        Double x = 250D;
        Double y = 250D;

        Baum baum = new Baum(x, y, baumart, 1);
        addTree(baum);

        return baum;
    }

    public Baum setSecondTree(Baum baum) {
        Baumart baumart = speciesGenerator.getNextSpecies();

        double radius = baumart.getRadius();
        double x = baum.getPosition().getX();
        double y = baum.getPosition().getY() + baum.getRadius() + radius;

        Baum secondBaum = new Baum(x, y, baumart, 2);
        addTree(secondBaum);
        return secondBaum;
    }

    private void addTrees() {
        Random r  = new Random();

        Baum firstTree = setFirstTree(r);
        Baum secondTree = setSecondTree(firstTree);

        baumPairList.add(new BaumPair(firstTree, secondTree));

        Integer maxTrees = 30;
        while (running && !baumPairList.isEmpty()) {


            if (Objects.equals(currentTree, maxTrees))
            {
                disable();

                allTrees.forEach((baum1) -> System.out.println(baum1.toString()));

                return;
            }

            Baumart baumart = speciesGenerator.getNextSpecies();
          // Baumart baumart = new Baumart(40.0, 7);
            BaumPair nextBaumPair = baumPairList.poll();

            Baum baumOne = nextBaumPair.getTreeOne();
            Baum baumTwo = nextBaumPair.getTreeTwo();

            System.out.println("Größe: " + baumPairList.size() );
            System.out.println("Baum1: " + baumOne.toString() );

            System.out.println("Baum2: " + baumTwo.toString() );

            for(int side=0; side<2; ++side)
            {
                Baum nextTree = setNextTree(baumOne, baumTwo, baumart, side);

                if (nextTree != null) {
                    System.out.println("setzte");
                    baumPairList.add(new BaumPair(baumOne, nextTree));
                    baumPairList.add(new BaumPair(baumTwo, nextTree));
                    addTree(nextTree);
                }
            }

            try {
                Thread.sleep(80);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

//    private Boolean checkCollision(Baum tree) {
//        for (Baum aBaum : allTrees) {
//            if (tree.checkCollision(aBaum)) {
//                System.out.println("Kollision! a: " + tree.getIndex() + " b: " + aBaum.getIndex());
//
//                return true;
//            }
//        }
//
//        return false;
//    }

    private Boolean checkCollision(Baum tree) {
        //return false;

        for (Baum aBaum : allTrees) {
            if (checkCollison(aBaum, tree)) {
                System.out.println("Kollision! a: " + tree.getIndex() + " b: " + aBaum.getIndex());

                return true;
            }
        }

        return false;

    }


    private boolean checkCollison(Baum treeOne, Baum newTree)
    {
        Double dx = treeOne.getCircle().getCenterX() - newTree.getCircle().getCenterX();
        Double dy = treeOne.getCircle().getCenterY() - newTree.getCircle().getCenterY();

        Double radiusSum = treeOne.getRadius() + newTree.getRadius();

        if(((dx * dx) + (dy * dy) - (radiusSum * radiusSum)) < 0)
        {
            return true;
        }
        return false;
    }

    private List<Baum> getLastTwoTrees() {
        List<Baum> lastTwoTrees = new ArrayList<>();

        if (allTrees.size() > 1) {
            lastTwoTrees.add(allTrees.get(allTrees.size() - 2));
            lastTwoTrees.add(allTrees.get(allTrees.size() - 1));
        }

        return lastTwoTrees;
    }

    private Boolean isInBounds(Baum tree) {

        Circle circle = tree.getCircle();

        Rectangle bounds = new Rectangle(width, height);

        return circle.getBoundsInParent().intersects(bounds.getBoundsInParent());
    }

    private void addTree(Baum tree) {
        Platform.runLater(() -> feld.addBaum(tree));
        allTrees.add(tree);
        currentTree++;
    }

    public static void disable() {
        running = false;
    }
}
