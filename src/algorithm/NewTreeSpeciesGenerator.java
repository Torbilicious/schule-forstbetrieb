package algorithm;

import model.Baumart;
import model.Feld;

import java.util.ArrayList;

/**
 * Created by TBinder on 03/02/16.
 */

public class NewTreeSpeciesGenerator {
    private final Feld feld;

    public NewTreeSpeciesGenerator(Feld feld) {
        this.feld = feld;
    }

    public Baumart getNextSpecies() {
        ArrayList<Baumart> species = feld.getBaumarten();
        Baumart speciesWithLowestAmountOfTrees = null;

        for (Baumart art : species) {
            if (speciesWithLowestAmountOfTrees == null)
                speciesWithLowestAmountOfTrees = art;

            if (art.getAnzahlBaeume() < speciesWithLowestAmountOfTrees.getAnzahlBaeume())
                speciesWithLowestAmountOfTrees = art;
        }

        return speciesWithLowestAmountOfTrees;
    }
}
