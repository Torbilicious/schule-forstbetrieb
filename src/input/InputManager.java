package input;

import javafx.geometry.Dimension2D;
import model.Baumart;
import model.Feld;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

/**
 * Created by TBinder on 25/01/16.
 * Edited by SMueller on 16/02/16.
 */

public class InputManager {
    public static Feld setupForestForInput(String input) throws FileEmpetyException {
        if (input.isEmpty()) {
            return getDefaultFeld();
        }

        ArrayList<String> lines = getAllLines(input);

        if (lines.size() == 1)
            return getDefaultFeld();

        Feld feld = new Feld(lines.get(0),
                getDimension(lines.get(1)));

//        Pattern pattern = new Pattern();
//        Pattern pattern = Pattern.compile("^\\d{1,3}\\.\\d{1,3}$");
//        Pattern pattern = Pattern.compile("(^\\d{1,3}\\.\\d{1,3})");
//        pattern.compile("^\\d{1,3}\\.\\d{1,3}$")

//        Pattern pattern = Pattern.compile("^\\d{1,3}\\.\\d{1,3}$");

        for (int i = 2; i < lines.size(); i++) {
            if (lines.get(i).matches("^\\d{1,3}\\.\\d{1,3}")) {
                double radius = Double.parseDouble(lines.get(i));
                feld.addBaumart(new Baumart(radius, (i - 2)));
            }
//            Matcher matcher = pattern.matcher(lines.get(i));
//            matcher.

        }

        System.out.println("Matches " + lines.get(2).matches("(^\\d{1,3}\\.\\d{1,3})"));

//        feld.addBaumart(new Baumart(20D, 0)); //temporary
//        feld.addBaumart(new Baumart(30D, 1));
//        feld.addBaumart(new Baumart(40D, 2));
//        feld.addBaumart(new Baumart(50D, 3));

        return feld;
    }

    private static Feld getDefaultFeld() {
        Feld feld = new Feld("No name specified", new Dimension2D(500, 500));
        feld.addBaumart(new Baumart(20D, 0));
        feld.addBaumart(new Baumart(30D, 1));
        feld.addBaumart(new Baumart(40D, 2));
        feld.addBaumart(new Baumart(50D, 3));

        return feld;
    }

    private static Dimension2D getDimension(String line) {
        int xSize = 0;
        int ySize = 0;

        if (line.isEmpty() || !line.matches("^\\d{1,4} \\d{1,4}$")) {// Unless size defined in line 1
            xSize = 500;
            ySize = 500;
        } else {
            String[] split = line.split(" ");
            xSize = Integer.parseInt(split[0]);
            ySize = Integer.parseInt(split[1]);
        }

        return new Dimension2D(xSize, ySize);
    }

    private static ArrayList<String> getAllLines(String input) {
        ArrayList<String> lines = new ArrayList<>();

        BufferedReader bufReader = new BufferedReader(new StringReader(input));

        String line;
        try {
            while ((line = bufReader.readLine()) != null)
                lines.add(line.split("%")[0]);//ignore comments starting with "%"
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }
}
